#pragma once
#include <cstdint>
#include <limits>


namespace swayapi {

	class IpcProto {
	public:

		enum class PayloadType {
			RUN_COMMAND = 0,
			GET_WORKSPACES = 1
		};

		IpcProto();
		~IpcProto();

		void set_payload(const char* payload, std::uint32_t size);
		void set_type(PayloadType type);
		const char* payload() const;
		const std::uint32_t payload_size() const;
		inline bool is_valid() { return !static_cast<bool>((_options >> 0) & 1); };

		
	protected:
		static constexpr std::uint32_t PAYLOAD_LENGTH_LIMIT = std::numeric_limits<std::uint32_t>::max();
		static constexpr std::uint8_t HEADER_LENGTH = 14;
		static constexpr std::uint8_t IPC_OPTION_INVALID = 1 << 0;
		static constexpr std::uint8_t IPC_OPTION_XXXXXX_X = 1 << 1;
		static constexpr std::uint8_t IPC_OPTION_XXXXX_XX = 1 << 2;
		static constexpr std::uint8_t IPC_OPTION_XXXX_XXX = 1 << 3;
		static constexpr std::uint8_t IPC_OPTION_XXX_XXXX = 1 << 4;
		static constexpr std::uint8_t IPC_OPTION_XX_XXXXX = 1 << 5;
		static constexpr std::uint8_t IPC_OPTION_X_XXXXXX = 1 << 6;
		static constexpr std::uint8_t IPC_OPTION__XXXXXXX = 1 << 7;
		std::uint8_t _options = 0;
		// "i3-ipc";uint32(payload length);uint32(payload type)
		unsigned char _header[HEADER_LENGTH];
		char* _payload;

		inline void _set_options(std::uint8_t options) { _options = options; };
		void _get(char*& ret_ipc, std::int64_t& ret_size) const;
		const std::uint32_t _get_type() const;
		void _set_int_type(std::uint32_t type);
		void _set_header_int(std::uint8_t start_pos, std::int32_t value);
	};
/*
class IPC {

	IPC();
	~IPC();

	virtual void set_type() = 0;
	virtual void set_payload() = 0;
	virtual bool is_valid() = 0;
};
*/

}
