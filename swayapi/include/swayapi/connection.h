#pragma once
#include <cstdint>
#include "swayapi/ipc-proto.h"


namespace swayapi {

class Connection {
public:
	Connection();
	~Connection();
	void init();

	/**
	* sandr 'send and recv' combines two functions together
	* using this method is more convinient but there is
	* double return value as value
	*/
	const IpcProto sandr(const IpcProto& ipc_proto) const;
	const bool send_ipc(const IpcProto& ipc_proto) const;
	const IpcProto recv_ipc() const;
private:
	std::int32_t _socket = 0;
	const char* socket_path() const;
};

}
