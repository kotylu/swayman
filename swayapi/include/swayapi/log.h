#pragma once
#include <cstdarg>
#include <cstdio>


namespace swayapi {
namespace log {

	enum class LogLevel {
		SILENT = 0,
		DEBUG = 1,
		TRACE = 2,
		WARN = 3,
		ERROR = 4
	};



	extern LogLevel log_level;
	static void set_log_level(LogLevel level);
	inline static void set_log_level(LogLevel log_level) { swayapi::log::log_level = log_level; }

	void _trace(const char* msg, ...);
	void _debug(const char* msg, ...);
	void _warn(const char* msg, ...);
	void _error(const char* msg, ...);

	static void _log(LogLevel level, FILE* stream, const char* msg, va_list args);

}
}

#define trace(message, ...) swayapi::log::_trace(message __VA_OPT__(, __VA_ARGS__))
#define debug(message, ...) swayapi::log::_debug(message __VA_OPT__(, __VA_ARGS__))
#define warn(message, ...) swayapi::log::_warn(message __VA_OPT__(, __VA_ARGS__))
#define error(message, ...) swayapi::log::_error(message __VA_OPT__(, __VA_ARGS__))
