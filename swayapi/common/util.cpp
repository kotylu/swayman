#include "common/util.h"
#include <cstdint>


std::uint32_t swayapi::util::uint_from_chararr(const unsigned char* chararr) {
	std::uint32_t result = 0;
	result |= (chararr[0] << 0);
	result |= (chararr[1] << 8);
	result |= (chararr[2] << 16);
	result |= (chararr[3] << 24);
	return result;
}

void swayapi::util::uint_to_charrarr(unsigned char* chararr, std::uint32_t value) {
	chararr[0] = (value >> 0)  & 0xFF;
	chararr[1] = (value >> 8)  & 0xFF;
	chararr[2] = (value >> 16) & 0xFF;
	chararr[3] = (value >> 24) & 0xFF;
}

std::int32_t swayapi::util::int_from_chararr(const char* chararr) {
	std::int32_t result = 0;
	result |= (chararr[0] << 0);
	result |= (chararr[1] << 8);
	result |= (chararr[2] << 16);
	result |= (chararr[3] << 24);
	return result;
}

void swayapi::util::int_to_charrarr(char* chararr, std::int32_t value) {
	chararr[3] = (value >> 24) & 0xFF;
	chararr[2] = (value >> 16) & 0xFF;
	chararr[1] = (value >> 8) & 0xFF;
	chararr[0] = value & 0xFF;
}

