#pragma once
#include <cstdint>


namespace swayapi {
namespace util {

/**
* int will be parsed from next 4 positions of chararr with left to right direction
* meaning ix=0 ~ 255, ix=1 ~ 65535, ...
*/
std::int32_t int_from_chararr(const char* chararr);
std::uint32_t uint_from_chararr(const unsigned char* chararr);
/**
* int will be inserted into next 4 positions of chararr with left to right direction
* meaning ix=0 ~ 255, ix=1 ~ 65535, ...
*/
void int_to_charrarr(char* chararr, std::int32_t value);
void uint_to_charrarr(unsigned char* chararr, std::uint32_t value);

}
}
