#include "swayapi/log.h"
#include <cstdarg>
#include <cstdio>
#include <cstring>


swayapi::log::LogLevel swayapi::log::log_level = swayapi::log::LogLevel::SILENT;

void swayapi::log::_log(LogLevel level, FILE* stream, const char* msg, va_list args) {
	const char* prompt;
	switch (level) {
		case LogLevel::TRACE:
			prompt = "[TRACE] \0";
			break;
		case LogLevel::DEBUG:
			prompt = "[DEBUG] \0";
			break;
		case LogLevel::WARN:
			prompt = "[WARN] \0";
			break;
		case LogLevel::ERROR:
			prompt = "[ERROR] \0";
			break;
		default:
			return;
	}
	// +2 - "\n\0"
	char final_msg[strlen(prompt) + strlen(msg) + 2];
	std::snprintf(final_msg, sizeof(final_msg), "%s%s\n", prompt, msg);
	std::vfprintf(stream, final_msg, args);
	va_end(args);
}

void swayapi::log::_trace(const char* msg, ...) {
	if ((int) swayapi::log::log_level > (int) LogLevel::TRACE) {
		return;
	}
	va_list args;
	va_start(args, msg);
	swayapi::log::_log(LogLevel::TRACE, stdout, msg, args);
}
void swayapi::log::_debug(const char* msg, ...) {
	if ((int) swayapi::log::log_level > (int) LogLevel::DEBUG) {
		return;
	}
	va_list args;
	va_start(args, msg);
	swayapi::log::_log(LogLevel::DEBUG, stdout, msg, args);
}

void swayapi::log::_warn(const char* msg, ...) {
	if ((int) swayapi::log::log_level > (int) LogLevel::WARN) {
		return;
	}
	va_list args;
	va_start(args, msg);
	swayapi::log::_log(LogLevel::WARN, stdout, msg, args);
}
void swayapi::log::_error(const char* msg, ...) {
	if ((int) swayapi::log::log_level > (int) LogLevel::ERROR) {
		return;
	}
	va_list args;
	va_start(args, msg);
	swayapi::log::_log(LogLevel::ERROR, stderr, msg, args);
}
