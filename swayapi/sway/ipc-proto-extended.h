#pragma once
#include "swayapi/ipc-proto.h"
#include <cstdint>


namespace swayapi {

/**
 * should be used only by library
 * this class 'extends' functionality of ipc-proto
 * that should not be accessible to user
 * like setting of ipc-proto raw options
 * and making some 'raw' methods public
 */
class IpcProtoExtended : public IpcProto {
	
	public:
		static constexpr std::uint32_t PAYLOAD_LENGTH_LIMIT = IpcProto::PAYLOAD_LENGTH_LIMIT;
		static constexpr std::uint8_t HEADER_LENGTH = IpcProto::HEADER_LENGTH;
		static constexpr std::uint8_t IPC_OPTION_INVALID = IpcProto::IPC_OPTION_INVALID;

		inline void set_options(std::uint8_t options) { _set_options(options); };
		inline void get(char*& ret_ipc, std::int64_t& ret_size) const { _get(ret_ipc, ret_size); };
		inline void set_int_type(std::uint32_t type) { _set_int_type(type); };
		inline const std::uint32_t get_type() const { return _get_type(); };
		inline void set_header_int(std::uint8_t start_pos, std::int32_t value) { _set_header_int(start_pos, value); };
};

}
