#include "swayapi/connection.h"
#include "sway/ipc-proto-extended.h"
#include "swayapi/ipc-proto.h"
#include "swayapi/log.h"
#include "common/util.h"
#include <cassert>
#include <cerrno>
#include <cstdint>
#include <sys/socket.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sys/un.h>
#include <unistd.h>


using namespace swayapi;

Connection::Connection() { }

Connection::~Connection() {
	close(_socket);
}

void Connection::init() {
	if (_socket != 0) {
		return;
	}
	const char* spath = socket_path();
	if (!spath) {
		error("Unable to find sway socket path");
		assert(false);
	}
	_socket = socket(AF_UNIX, SOCK_STREAM, 0);
	sockaddr_un addr;
	addr.sun_family = AF_UNIX;
	// if the path is longer than addr.sun_path size
	// let it fail on connection
	strncpy(addr.sun_path, spath, sizeof(addr.sun_path) - 1);
	if (strlen(spath) >= sizeof(addr.sun_path)) {
		error("Provided path %s is longer than socket path limit [%d]", spath, sizeof(addr.sun_path));
	}

	std::int32_t connect_result = connect(_socket, (sockaddr*)&addr, sizeof(sockaddr_un));
	if (connect_result < 0) {
		error("Unable to connect to swaysock\npath: %s\nerrno: %d", addr.sun_path, errno);
		assert(false);
	}
	trace("Connection Succesfull [%s]", addr.sun_path);
}

const char* Connection::socket_path() const {
	// Trying ENVIRONMENT
	char* _sway_sock = std::getenv("SWAYSOCK");
	if (_sway_sock || strlen(_sway_sock) > 0) {
		trace("swaysock path[ENV]: %s", _sway_sock);
		return _sway_sock;
	}

	// Trying CMD
	FILE* cmd = popen("sway --get-socketpath 2>/dev/null", "r")	;
	char* line = nullptr;
	size_t line_size = 0;
	if (cmd) {
		ssize_t bytes_read = getline(&line, &line_size, cmd);
		pclose(cmd);
		if (bytes_read > 0) {
			if (line[bytes_read - 1] == '\n') {
				line[bytes_read - 1] = '\0';
			}
			trace("swaysock path[CMD]: %s", line);
			return line;
		}
		free(line);
	}
	error("Unable to find swaysock path");
	return nullptr;
}
const IpcProto Connection::sandr(const IpcProto& ipc_proto) const {
	if (send_ipc(ipc_proto)) {
		return recv_ipc();
	}
	error("sandr did not execute properly");
	IpcProtoExtended err_ipc;
	err_ipc.set_options(IpcProtoExtended::IPC_OPTION_INVALID);
	return err_ipc;
}

const bool Connection::send_ipc(const IpcProto& ipc_proto) const {
	char* ipc_str;
	std::int64_t ipc_str_size;

	const IpcProtoExtended* internal_ipc = static_cast<const IpcProtoExtended*>(&ipc_proto);
	if (!internal_ipc) {
		return 0;
		error("[send] Unable to cast IpcProto to IpcProtoInternal");
	}
	internal_ipc->get(ipc_str, ipc_str_size);
	trace("Sending ipc [type=%u]", internal_ipc->get_type());
	std::int64_t bytes_sent = send(_socket, ipc_str, ipc_str_size, 0);
	if (bytes_sent < 0) {
		error("Unable to send data. Errno: %d", errno);
		return 0;
	}
	return bytes_sent > 0;
}

const IpcProto Connection::recv_ipc() const {
	char buffer[IpcProtoExtended::HEADER_LENGTH];
	recv(_socket, buffer, 6, 0);

	unsigned char tmp[4];
	recv(_socket, tmp, 4, 0);
	std::uint32_t payload_length = swayapi::util::uint_from_chararr(tmp);

	recv(_socket, tmp, 4, 0);
	std::uint32_t message_type = swayapi::util::uint_from_chararr(tmp);

	char payload[payload_length];
	recv(_socket, payload, payload_length, 0);

	trace("recieved ipc [type=%u]", message_type);

	IpcProtoExtended result;
	result.set_int_type(message_type);
	result.set_payload(payload, payload_length);
	return result;
}

