#include "common/util.h"
#include "swayapi/ipc-proto.h"
#include "swayapi/log.h"
#include <cmath>
#include <cstring>
#include <assert.h>


using namespace swayapi;

IpcProto::IpcProto() {
	std::memset(_header, 0, HEADER_LENGTH);
	std::memcpy(_header, "i3-ipc", 6);
	_payload = nullptr;
}
IpcProto::~IpcProto() {
	if (!_payload) {
		delete _payload;
	}
}

const std::uint32_t IpcProto::payload_size() const {
	return swayapi::util::uint_from_chararr(_header + 6);
}

const char* IpcProto::payload() const {
	return _payload;
}

void IpcProto::_get(char*& ret_ipc, std::int64_t& ret_size) const {
	const std::uint32_t p_length = swayapi::util::uint_from_chararr(_header + 6);
	char* ipc = new char[HEADER_LENGTH + p_length];
	std::memcpy(ipc, _header, HEADER_LENGTH);
	std::memcpy(ipc+HEADER_LENGTH, _payload, p_length);
	ret_ipc = ipc;
	ret_size = HEADER_LENGTH + p_length;
}

void IpcProto::_set_header_int(std::uint8_t pos, std::int32_t value) {
	swayapi::util::uint_to_charrarr(_header + pos, value);
}

const std::uint32_t IpcProto::_get_type() const {
	return swayapi::util::uint_from_chararr(_header + 10);
}

void IpcProto::set_payload(const char* payload, std::uint32_t size) {
	if (!payload) {
		error("Payload can not be nullptr");
		return;
	}
	if (size > PAYLOAD_LENGTH_LIMIT) {
		error("Payload is longer than limit %u", PAYLOAD_LENGTH_LIMIT);
		assert(false);
	}
	if (_payload == nullptr || size > payload_size()) {
		if (_payload != nullptr) {
			assert(false);
			delete(_payload);
		}
		_payload = new char[size];
	}
	else {
		std::memset(_payload, 0, payload_size());
	}
	std::memcpy(_payload, payload, size);
	_set_header_int(6, size);
}

void IpcProto::set_type(PayloadType type) {
	std::int32_t p_type = static_cast<std::int32_t>(type);
	_set_header_int(10, p_type);
}

void IpcProto::_set_int_type(std::uint32_t type) {
	_set_header_int(10, type);
}
