#include <cstdint>
#include <cstdio>
#include <cstring>
#include <endian.h>
#include <iostream>
#include "swayapi/connection.h"
#include "swayapi/ipc-proto.h"
#include "swayapi/log.h"


int main(int argc, char** argv) {
	swayapi::log::set_log_level(swayapi::log::LogLevel::DEBUG);
	swayapi::Connection con{};
	con.init();
	
	swayapi::IpcProto ipc_req;
	ipc_req.set_type(swayapi::IpcProto::PayloadType::GET_WORKSPACES);
	if (!con.send_ipc(ipc_req)) {
		error("Error during sending ipc");
	};
	swayapi::IpcProto ipc_res = con.recv_ipc();
	if (ipc_res.is_valid() == false) {
		error("Did not recieve valid IPC");
		return 1;
	}
	for (std::uint32_t i = 0; i < ipc_res.payload_size(); i++) {
		std::cout << ipc_res.payload()[i];
	}


	return 0;
}

